package org.ishara.ses.service;

import org.bson.Document;
import org.ishara.ses.model.Student;
import org.ishara.ses.mongoDb.DbOperationClass;

public class StudentDetailsService {
    DbOperationClass db= new DbOperationClass();

    public int addStudent(Student ss) throws InterruptedException {

        db.addStudent(ss);

        return 1;

    }

    public boolean loginStudent(String uname, String pw) throws InterruptedException {

        db.verifyStudent(uname,pw);
       // System.out.println(n);

        return true;
    }

    public void updateStudent(Document d, Student s) throws InterruptedException {

        db.updateStudent(d,s);
    }

    public void enrollCourse(Document d, String courseId) throws InterruptedException {
        db.enrollCourse(d,courseId);
    }
}
