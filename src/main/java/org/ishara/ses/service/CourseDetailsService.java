package org.ishara.ses.service;

import org.ishara.ses.model.Course;
import org.ishara.ses.mongoDb.DbOperationClass;

public class CourseDetailsService {

    DbOperationClass db= new DbOperationClass();

    public void addCourse(Course c) throws InterruptedException {
        db.addCourse(c);

    }

    public void updateCourse(Course c) throws InterruptedException {
        db.updateCourse(c);
    }

    public void viewAllCources() throws InterruptedException {
        db.viewAllCourses();
    }
}
