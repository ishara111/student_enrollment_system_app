package org.ishara.ses.resource;

import org.ishara.ses.model.Course;
import org.ishara.ses.service.CourseDetailsService;

import java.util.Scanner;

public class CourseResource {
    Scanner sc= new Scanner(System.in);
    CourseDetailsService cd = new CourseDetailsService();


    public int getOption() {
        System.out.println("Select one option: ");
        System.out.println("  Add Courses ---> 1");
        System.out.println("  Update Courses --->2 ");
        System.out.println("  Set GPA --->3 ");
        System.out.println("  View all courses --->4 ");

        int op = sc.nextInt();

        return op;
    }

    public void addCourse() throws InterruptedException {

            System.out.println("Enter Course Id:");
            String cid = sc.next();

            System.out.println("Enter Course Name:");
            String course_name = sc.next();

            System.out.println("Enter Professor Name:");
            String prof_name = sc.next();

            System.out.println("Enter Location:");
            String location = sc.next();

            System.out.println("Enter Credits:");
            double credit = sc.nextDouble();

            Course c = new Course(cid,course_name,prof_name,location,credit);
            cd.addCourse(c);


    }

    public void updateCourse() throws InterruptedException {

        System.out.println("Enter Course Id:");
        String cid = sc.next();

        System.out.println("Update the fields :");
        System.out.println("Enter Course Name:");
        String course_name = sc.next();

        System.out.println("Enter Professor Name:");
        String prof_name = sc.next();

        System.out.println("Enter Location:");
        String location = sc.next();

        System.out.println("Enter Credits:");
        double credit = sc.nextDouble();

        Course c = new Course(cid,course_name,prof_name,location,credit);
        cd.updateCourse(c);


    }

    public void setGPA(){


    }

    public void viewAllCourses() throws InterruptedException {
        cd.viewAllCources();

    }


}
