package org.ishara.ses.resource;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Scanner sc= new Scanner(System.in);
        //StudentDetailsService sd= new StudentDetailsService();
        StudentResource sr= new StudentResource();
        CourseResource cr = new CourseResource();

        System.out.println("Enter the mode:");
        System.out.println("Student ----> 1");
        System.out.println("Admin ----> 2");

        int respo=  sc.nextInt();

        if(respo ==1){
            int op=sr.getOption();

            switch(op){
                case 1: sr.regStudent();
                        break;
                case 2: sr.loginStudent();
                        break;
            }
        }


        else if(respo ==2){
            int op=cr.getOption();

            switch(op){
                case 1: cr.addCourse();
                        break;
                case 2: cr.updateCourse();
                        break;
                case 3: cr.setGPA();
                        break;
                case 4: cr.viewAllCourses();
                        break;
            }

        }

        else{


        }




    }
}
