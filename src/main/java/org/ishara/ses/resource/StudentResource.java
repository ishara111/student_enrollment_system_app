package org.ishara.ses.resource;

import org.bson.Document;
import org.ishara.ses.model.Student;
import org.ishara.ses.service.StudentDetailsService;

import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

public class StudentResource {
    protected final static CountDownLatch doneLatch = new CountDownLatch(1);
    Scanner sc= new Scanner(System.in);
    StudentDetailsService sd= new StudentDetailsService();
    Document current;
    CourseResource cr= new CourseResource();

    public StudentResource(){

    }



    public int getOption(){
        System.out.println("Select one option: ");
        System.out.println("  Register ---> 1");
        System.out.println("  Login --->2 ");

        int op = sc.nextInt();

        return op;
    }

    public void getOption2(Document d) throws InterruptedException {
        System.out.println("Select one option: ");
        System.out.println("  Update ---> 1");
        System.out.println("  Enroll ---> 2");

        int op = sc.nextInt();

        if(op ==1)
            updateStudent(d);

        else if(op == 2)
            enrollCourse(d);
    }


    public int regStudent() throws InterruptedException {
        System.out.println("Enter First Name:");
        String fname = sc.next();

        System.out.println("Enter Last Name:");
        String lname = sc.next();

        System.out.println("Enter User Name:");
        String user_name = sc.next();

        System.out.println("Enter Password:");
        String pw = sc.next();

        System.out.println("Enter Email:");
        String em = sc.next();

        System.out.println("Enter phone:");
        String ph = sc.next();

        Student s= new Student(fname,lname,user_name,pw,em,ph);
        sd.addStudent(s);

        return 1;
    }

    public int loginStudent() throws InterruptedException {
        System.out.println("Enter UserName:");
        String user_name = sc.next();
        System.out.println("Enter password:");
        String pw = sc.next();

        sd.loginStudent(user_name,pw);

    return 1;
    }


    public int updateStudent(Document d) throws InterruptedException {
        System.out.println("Enter First Name:");
        String fname = sc.next();

        System.out.println("Enter Last Name:");
        String lname = sc.next();

        System.out.println("Enter User Name:");
        String user_name = sc.next();

        System.out.println("Enter Password:");
        String pw = sc.next();

        System.out.println("Enter Email:");
        String em = sc.next();

        System.out.println("Enter phone:");
        String ph = sc.next();

        Student s= new Student(fname,lname,user_name,pw,em,ph);
        sd.updateStudent(d,s);


        return 1;

    }

    public void enrollCourse(Document d) throws InterruptedException { System.out.println("Select one course.....\n");
        System.out.println("----------------------------------------------");

        //cr.viewAllCourses();
        //doneLatch.await();
        //System.out.println("\n --------------------------------------------");
        System.out.println("Enter Course Id:");
        String cid = sc.next();

        sd.enrollCourse(d,cid);

    }



}
