package org.ishara.ses.mongoDb;

public class Enrollment {
    private double grade;
    private long student_id;
    private long course_id;

    public Enrollment(){ }

    public Enrollment(double grade,long student_id,long course_id){
        this.grade = grade;
        this.student_id = student_id;
        this.course_id = course_id;
    }

    public Enrollment(long student_id,long course_id){
        this.student_id = student_id;
        this.course_id = course_id;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public long getStudent_id() {
        return student_id;
    }

    public void setStudent_id(long student_id) {
        this.student_id = student_id;
    }

    public long getCourse_id() {
        return course_id;
    }

    public void setCourse_id(long course_id) {
        this.course_id = course_id;
    }
}
