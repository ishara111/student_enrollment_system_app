package org.ishara.ses.mongoDb;

import com.mongodb.Block;
import com.mongodb.async.SingleResultCallback;
import com.mongodb.async.client.MongoClients;


import com.mongodb.async.client.MongoCollection;
import com.mongodb.async.client.MongoDatabase;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.ishara.ses.model.Course;
import org.ishara.ses.model.Student;

import java.util.ArrayList;
import java.util.List;

import org.ishara.ses.resource.CourseResource;
import org.ishara.ses.resource.StudentResource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CountDownLatch;


import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.currentDate;
import static com.mongodb.client.model.Updates.set;
import static org.ishara.ses.mongoDb.DbConnection.mongoClient;

public class DbOperationClass {
    static MongoDatabase database;

    protected final static CountDownLatch doneLatch = new CountDownLatch(1);


    static SingleResultCallback<Document> callbackPrintDocuments = new SingleResultCallback<Document>() {
        @Override
        public void onResult(final Document document, final Throwable t) {
            System.out.println(document.toJson());
        }
    };

    static SingleResultCallback<Void> callbackWhenFinished = new SingleResultCallback<Void>() {
        @Override
        public void onResult(final Void result, final Throwable t) {
            System.out.println("Operation Finished!");
        }
    };

    static Block<Document> printDocumentBlock = new Block<Document>() {
        @Override
        public void apply(final Document document) {
            System.out.println(document.toJson());
        }
    };



    public DbOperationClass(){
        database = mongoClient.getDatabase("enrollDb");
    }


    public static int addStudent(Student s) throws InterruptedException {

        //Creating a collection
        MongoCollection<Document> collection = database.getCollection("Student");

        //database.createCollection("Student");

        Document doc = new Document("id", s.getStudentId())
                .append("userName", s.getUsername())
                .append("password", s.getPassword())
                .append("fname", s.getfName() )
                .append("lname", s.getlName())
                .append("email", s.getEmail())
                .append("phone", s.getPhone());

       // collection.insertOne(doc);

        collection.insertOne(doc, new SingleResultCallback<Void>() {
            @Override
            public void onResult(final Void result, final Throwable t) {
                System.out.println("Registration Successful...!");
            }
        });
        doneLatch.await();
        //while(true){}
        return 1;
    }


    public static boolean verifyStudent(final String uname, final String pw) throws InterruptedException {
        final StudentResource sr= new StudentResource();

        MongoCollection<Document> students = database.getCollection("Student");
        //FindIterable<Document> studDataList =  students.find();


        final int state;
        final List<Document> myList = new ArrayList<Document>();
        students.find().into(myList,
                new SingleResultCallback<List<Document>>() {
                    @Override
                    public void onResult(final List<Document> result, final Throwable t) {
                        System.out.println("Operation Finished and myList now contains the results");
                        for (int i = 0; i < myList.size(); i++) {
                            //System.out.println(studDataList.get(i));

                            Document document = myList.get(i);
                            //System.out.println(document.get("_id")+ " :"+document.get("userName")+ " : "+document.get("password"));

                            if((document.get("userName")).equals(uname) && (document.get("password")).equals(pw)){

                                System.out.println("Login Successful..");
                                try {
                                    sr.getOption2(document);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                doneLatch.countDown();

                            }
                            else if(i== (myList.size()-1)){
                                try {
                                    sr.loginStudent();
                                    doneLatch.countDown();
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }

                        }
                    }

                }
                );




        doneLatch.await();
        /*
        MongoCollection<org.bson.Document> coll = database.getCollection("Student");
        List<org.bson.Document> studDataList = coll.find().forEach(printDocumentBlock, callbackWhenFinished);
        */
        //client.close();
        return true;


    }


    public static void updateStudent(final Document d, Student s) throws InterruptedException {
        MongoCollection<Document> students = database.getCollection("Student");
        final StudentResource sr = new StudentResource();
        final Document doc = d;
        //update a student
        students.updateOne(
                eq("_id", new ObjectId(String.valueOf(d.get("_id")))),
                combine(set("userName", s.getUsername()),
                        set("password", s.getPassword()),
                        set("fname", s.getfName() ),
                        set("lname", s.getlName()),
                        set("email", s.getEmail()),
                        set("phone", s.getPhone()),
                        currentDate("lastModified")),
                new SingleResultCallback<UpdateResult>() {
                    @Override
                    public void onResult(final UpdateResult result, final Throwable t) {
                        //System.out.println(result.getModifiedCount());
                        System.out.println("Student updated Successfully...!");
                        System.out.println("\n");
                        try {
                            sr.getOption2(doc);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
        doneLatch.await();


    }


    public static void countStudent(){

        MongoCollection<Document> students = database.getCollection("Student");
        students.count(
                new SingleResultCallback<Long>() {
                    @Override
                    public void onResult(final Long count, final Throwable t) {
                        System.out.println(count);

                    }
                });
    }



    public void addCourse(Course c) throws InterruptedException {
        MongoCollection<Document> collection = database.getCollection("Course");

        Document doc = new Document("courseId", c.getCourseId())
                .append("courseName", c.getCourseName())
                .append("profName", c.getProfName())
                .append("location", c.getLocation() )
                .append("credit", c.getCredit());


        collection.insertOne(doc, new SingleResultCallback<Void>() {
            @Override
            public void onResult(final Void result, final Throwable t) {
                System.out.println("Inserted...!");
            }
        });
        doneLatch.await();

        //while(true){}
        return ;


    }


    public void updateCourse(Course c) throws InterruptedException {
        MongoCollection<Document> Courses = database.getCollection("Course");
        final CourseResource cr = new CourseResource();
        //update a course
        Courses.updateOne(
                eq("courseId", c.getCourseId()),
                combine(set("courseName", c.getCourseName()),
                        set("profName", c.getProfName()),
                        set("location", c.getLocation()  ),
                        set("credit", c.getCredit()),
                        currentDate("lastModified")),
                new SingleResultCallback<UpdateResult>() {
                    @Override
                    public void onResult(final UpdateResult result, final Throwable t) {
                        //System.out.println(result.getModifiedCount());
                        System.out.println("Course updated Successfully...!");
                        System.out.println("\n");
                        cr.getOption();

                    }
                });
        doneLatch.await();


    }


    public void viewAllCourses() throws InterruptedException {
        MongoCollection<Document> Courses = database.getCollection("Course");
        Courses.find().forEach(printDocumentBlock, callbackWhenFinished);
        doneLatch.await();

    }

    public void enrollCourse(Document d,String courseId) throws InterruptedException {
        MongoCollection<Document> collection = database.getCollection("Enrollment");
        viewAllCourses();
        Document doc = new Document("courseId", courseId)
                .append("StudentId", d.get("_id"))
                .append("grade", 0);


        collection.insertOne(doc, new SingleResultCallback<Void>() {
            @Override
            public void onResult(final Void result, final Throwable t) {
                System.out.println("Inserted...!");
            }
        });
        doneLatch.await();


        //while(true){}
        return ;
    }
}
